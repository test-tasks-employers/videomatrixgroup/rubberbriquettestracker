using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RubberBriquettesTrackerReporter.DAL;
using RubberBriquettesTrackerReporter.Models;

namespace RubberBriquettesTrackerReporter.RequestHandlers
{
    /// <summary>
    /// Обработчик запроса получения отчета
    /// </summary>
    public class ReportRequestHandler : IRequestHandler<ReportQuery, List<ReportItem>>
    {
        private readonly RubberBriquettesTrackerContext _context;

        /// <summary>
        /// Конструкток
        /// </summary>
        /// <param name="context">БД контект</param>
        public ReportRequestHandler(RubberBriquettesTrackerContext context)
        {
            _context = context;
        }

        /// <inheritdoc />
        public async Task<List<ReportItem>> Handle(ReportQuery request, CancellationToken cancellationToken)
        {
            var dateCondition = WhereDateCondition(request);
            var tasks = Zone.DefaultZones.Select(async _ => new ReportItem()
            {
                ZoneName = _.Name,
                ZoneBriquettesNumber =
                    await _context.ObjectTrack.CountAsync(dateCondition.And(WhereCoordinateCondition(_)),
                        cancellationToken: cancellationToken)
            });

            var res = await Task.WhenAll(tasks);
            return res.ToList();
        }

        private static Expression<Func<ObjectTrack, bool>> WhereDateCondition(ReportQuery request)
        {
            return o => (request.Start == null || o.CrtDate >= request.Start) &&
                        (request.End == null || o.CrtDate <= request.End);
        }

        private static Expression<Func<ObjectTrack, bool>> WhereCoordinateCondition(Zone zone)
        {
            return o => o.X >= zone.StartX && o.X <= zone.EndX;
        }
    }
}