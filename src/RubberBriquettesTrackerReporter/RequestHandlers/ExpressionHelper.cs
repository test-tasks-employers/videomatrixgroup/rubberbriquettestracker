using System;
using System.Linq.Expressions;

namespace RubberBriquettesTrackerReporter.RequestHandlers
{
    /// <summary>
    ///     Хелперы для работы с Expression Trees
    /// </summary>
    public static class ExpressionHelper
    {
        /// <summary>
        ///     Композиция двух логических выражений c объектом по условию "и".
        ///     Предназначено для конструирования критериев выборки в IEnumerable и IQueryable
        /// </summary>
        /// <param name="left">левая часть выражения</param>
        /// <param name="right">правая часть выражения</param>
        /// <returns>
        ///     Композицию <paramref name="left" /> и <paramref name="right" />
        /// </returns>
        /// <typeparam name="T">Тип объекта</typeparam>
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> left,
            Expression<Func<T, bool>> right)
        {
            if (left == null)
            {
                return right;
            }

            if (right == null)
            {
                return left;
            }

            var invokedExpr = Expression.Invoke(right, left.Parameters);
            return Expression.Lambda<Func<T, bool>>(
                Expression.AndAlso(left.Body, invokedExpr), left.Parameters);
        }
    }
}