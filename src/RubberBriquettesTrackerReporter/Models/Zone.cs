
using System.Collections.Generic;
using RubberBriquettesTrackerReporter.Resources;

namespace RubberBriquettesTrackerReporter.Models
{
    /// <summary>
    /// Зона
    /// </summary>
    public class Zone
    {
        /// <summary>
        /// Зона штатного появления 1
        /// </summary>
        public static readonly Zone ZoneOfRegularAppearance1 = new Zone(Zones.ZoneOfRegularAppearance1, 5100, 5397);
        /// <summary>
        /// Зона штатного появления 2
        /// </summary>
        public static readonly Zone ZoneOfRegularAppearance2 = new Zone(Zones.ZoneOfRegularAppearance2, 4863, 5013);
        /// <summary>
        /// Спуск конвейера
        /// </summary>
        public static readonly Zone BeltDescent = new Zone(Zones.BeltDescent, 2729, 2978);
        /// <summary>
        /// Весы
        /// </summary>
        public static readonly Zone Libra = new Zone(Zones.Libra, 2008, 2214);
        /// <summary>
        /// Брак по весу
        /// </summary>
        public static readonly Zone MarriageByWeight = new Zone(Zones.MarriageByWeight, 1688, 1996);
        /// <summary>
        /// Металлодетектор
        /// </summary>
        public static readonly Zone MetalDetector = new Zone(Zones.MetalDetector, 1203, 1660);
        /// <summary>
        /// Брак по металлу
        /// </summary>
        public static readonly Zone MarriageByMetal = new Zone(Zones.MarriageByMetal, 878, 1186);
        /// <summary>
        /// Пленкообертка
        /// </summary>
        public static readonly Zone SkinWrapper = new Zone(Zones.SkinWrapper, 325, 725);
        /// <summary>
        /// Штатный уход
        /// </summary>
        public static readonly Zone RegularExit = new Zone(Zones.RegularExit, 30, 290);

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Название зоны</param>
        /// <param name="startX">Начальная координата</param>
        /// <param name="endX">Конечная координата</param>
        public Zone(string name, int startX, int endX)
        {
            Name = name;
            StartX = startX;
            EndX = endX;
        }
        
        /// <summary>
        /// Название зоны
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Начальная координата
        /// </summary>
        public int StartX { get; private set; }
        /// <summary>
        /// Конечная координата
        /// </summary>
        public int EndX { get; private set; }

        /// <summary>
        /// Предустановленые зоны, хорошо бы хранить в БД
        /// </summary>
        public static readonly ICollection<Zone> DefaultZones = new List<Zone>
        {
            ZoneOfRegularAppearance1,
            ZoneOfRegularAppearance2,
            BeltDescent,
            Libra,
            MarriageByWeight,
            MetalDetector,
            MarriageByMetal,
            SkinWrapper,
            RegularExit,
        };
    }
}