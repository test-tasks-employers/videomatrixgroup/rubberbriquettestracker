namespace RubberBriquettesTrackerReporter.Models
{
    /// <summary>
    /// Элемент отчета
    /// </summary>
    public class ReportItem
    {
        /// <summary>
        /// Название зоны
        /// </summary>
        public string ZoneName { get; set; }
        
        /// <summary>
        /// Количество прошедших брикетов через зону
        /// </summary>
        public long ZoneBriquettesNumber { get; set; }
    }
}