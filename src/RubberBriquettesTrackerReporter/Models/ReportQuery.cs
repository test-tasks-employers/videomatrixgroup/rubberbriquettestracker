using System;
using System.Collections.Generic;
using MediatR;

namespace RubberBriquettesTrackerReporter.Models
{
    /// <summary>
    /// Запрос отчета
    /// </summary>
    public class ReportQuery : IRequest<List<ReportItem>>
    {
        /// <summary>
        /// "С"
        /// </summary>
        public DateTime? Start { get; set; }
        
        /// <summary>
        /// "По"
        /// </summary>
        public DateTime? End { get; set; }
    }
}