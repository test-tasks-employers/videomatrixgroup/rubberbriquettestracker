﻿var reportPageApp = (function () {
    var appReportVm = {
        items: [],
        filter: {
            Start:"",
            End:""
        }
    };
    var appApartmentList = new Vue({
        el: '#reportArea',
        data: appReportVm,
        methods: {
            get: function () {
                $('#getReportBtn').prop("disabled", true);
                if(this.$ErrorLabel){
                    this.$ErrorLabel.remove();
                }
                axios.get('/home/get', {params: appReportVm.filter})
                    .then(function (response) {
                        if (response.data.some(function (value) {
                            return value.zoneBriquettesNumber > 0; })){
                            appReportVm.items = response.data;
                        }
                        else {
                            appReportVm.items = null;
                            this.$ErrorLabel = $("<label>Даных нет для указанного периода</label>");
                            $("#reportTable").after(this.$ErrorLabel);
                        }
                        $('#getReportBtn').prop("disabled", false);
                    })
                    .catch(function (error) {
                        console.log(error);
                        $('#getReportBtn').prop("disabled", false);
                    });
            },
        },
        mounted : function() {
        },
        components: {
            vuejsDatepicker
        }
    });
    return {
        init: function () {
            

        }
    };
})();