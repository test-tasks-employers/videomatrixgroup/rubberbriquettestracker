﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using RubberBriquettesTrackerReporter.DAL;
using RubberBriquettesTrackerReporter.Models;
using RubberBriquettesTrackerReporter.RequestHandlers;

namespace RubberBriquettesTrackerReporter.Controllers
{
    /// <summary>
    /// Контроллер
    /// </summary>
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="mediator">CQRS</param>
        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// Метод для начаьной загрузки
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            ViewBag.Title = Resources.View.ReportPageHeader;
            return View();
        }

        /// <summary>
        /// Получить отчет
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <returns>Список элементов отчета</returns>
        [HttpGet]
        public async Task<ActionResult> Get(ReportQuery query)
        {
            var response = await _mediator.Send(query ?? new ReportQuery());
            return Ok(response);
        }

        public IActionResult Error(string errorId)
        {
            var pathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            Exception exception = pathFeature?.Error; // Here will be the exception details.

            return View();
        }
    }
}