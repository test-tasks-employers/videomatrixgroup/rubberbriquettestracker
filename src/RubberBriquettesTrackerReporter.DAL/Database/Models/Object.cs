﻿using System;
using System.Collections.Generic;

namespace RubberBriquettesTrackerReporter.DAL
{
    public partial class Object
    {
        public Object()
        {
            ObjectStatus = new HashSet<ObjectStatus>();
            ObjectTrack = new HashSet<ObjectTrack>();
        }

        public int Id { get; set; }
        public int ModuleId { get; set; }
        public DateTime CrtDate { get; set; }

        public virtual Module Module { get; set; }
        public virtual ICollection<ObjectStatus> ObjectStatus { get; set; }
        public virtual ICollection<ObjectTrack> ObjectTrack { get; set; }
    }
}
