﻿using System;
using System.Collections.Generic;

namespace RubberBriquettesTrackerReporter.DAL
{
    public partial class ObjectStatus
    {
        public int Id { get; set; }
        public int ObjectId { get; set; }
        public int StatusTypeId { get; set; }
        public DateTime CrtDate { get; set; }

        public virtual Object Object { get; set; }
        public virtual StatusType StatusType { get; set; }
    }
}
