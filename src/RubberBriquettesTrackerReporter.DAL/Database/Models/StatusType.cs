﻿using System;
using System.Collections.Generic;

namespace RubberBriquettesTrackerReporter.DAL
{
    public partial class StatusType
    {
        public StatusType()
        {
            ObjectStatus = new HashSet<ObjectStatus>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ObjectStatus> ObjectStatus { get; set; }
    }
}
