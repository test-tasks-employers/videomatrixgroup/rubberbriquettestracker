﻿using System;
using System.Collections.Generic;

namespace RubberBriquettesTrackerReporter.DAL
{
    public partial class ObjectTrack
    {
        public int Id { get; set; }
        public int ObjectId { get; set; }
        public virtual int? X { get; set; }
        public int? Y { get; set; }
        public virtual DateTime CrtDate { get; set; }

        public virtual Object Object { get; set; }
    }
}
