﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RubberBriquettesTrackerReporter.DAL
{
    public partial class RubberBriquettesTrackerContext : DbContext
    {
        public RubberBriquettesTrackerContext()
        {
        }

        public RubberBriquettesTrackerContext(DbContextOptions<RubberBriquettesTrackerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Module> Module { get; set; }
        public virtual DbSet<Object> Object { get; set; }
        public virtual DbSet<ObjectStatus> ObjectStatus { get; set; }
        public virtual DbSet<ObjectTrack> ObjectTrack { get; set; }
        public virtual DbSet<StatusType> StatusType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Module>(entity =>
            {
                entity.ToTable("module", "main");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('main.module_id_seq'::regclass)");

                entity.Property(e => e.Guid)
                    .HasColumnName("guid")
                    .HasColumnType("character varying");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Object>(entity =>
            {
                entity.ToTable("object", "main");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('main.object_id_seq'::regclass)");

                entity.Property(e => e.CrtDate).HasColumnName("crt_date");

                entity.Property(e => e.ModuleId).HasColumnName("module_id");

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.Object)
                    .HasForeignKey(d => d.ModuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("object_module_id_fkey");
            });

            modelBuilder.Entity<ObjectStatus>(entity =>
            {
                entity.ToTable("object_status", "main");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('main.object_status_id_seq'::regclass)");

                entity.Property(e => e.CrtDate).HasColumnName("crt_date");

                entity.Property(e => e.ObjectId).HasColumnName("object_id");

                entity.Property(e => e.StatusTypeId).HasColumnName("status_type_id");

                entity.HasOne(d => d.Object)
                    .WithMany(p => p.ObjectStatus)
                    .HasForeignKey(d => d.ObjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("object_status_object_id_fkey");

                entity.HasOne(d => d.StatusType)
                    .WithMany(p => p.ObjectStatus)
                    .HasForeignKey(d => d.StatusTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("object_status_status_type_id_fkey");
            });

            modelBuilder.Entity<ObjectTrack>(entity =>
            {
                entity.ToTable("object_track", "main");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('main.object_track_id_seq'::regclass)");

                entity.Property(e => e.CrtDate).HasColumnName("crt_date");

                entity.Property(e => e.ObjectId).HasColumnName("object_id");

                entity.Property(e => e.X).HasColumnName("x");

                entity.Property(e => e.Y).HasColumnName("y");

                entity.HasOne(d => d.Object)
                    .WithMany(p => p.ObjectTrack)
                    .HasForeignKey(d => d.ObjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("object_track_object_id_fkey");
            });

            modelBuilder.Entity<StatusType>(entity =>
            {
                entity.ToTable("status_type", "main");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('main.status_type_id_seq'::regclass)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");
            });

            modelBuilder.HasSequence("module_id_seq");

            modelBuilder.HasSequence("object_id_seq");

            modelBuilder.HasSequence("object_status_id_seq");

            modelBuilder.HasSequence("object_track_id_seq");

            modelBuilder.HasSequence("status_type_id_seq");
        }
    }
}
