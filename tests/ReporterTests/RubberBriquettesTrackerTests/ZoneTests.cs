using System.Collections;
using System.Globalization;
using System.Linq;
using NUnit.Framework;
using RubberBriquettesTrackerReporter.Models;

namespace Tests
{
    [TestFixture]
    public class ZoneTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void DefaultZonesTest()
        {
            var result = Zone.DefaultZones;
            var resourceSet = RubberBriquettesTrackerReporter.Resources.Zones.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture,
                true, true).Cast<DictionaryEntry>().ToDictionary(r=>r.Key.ToString(), r=>r.Value.ToString());
            Assert.AreEqual(resourceSet.Count(), result.Count);
            Assert.IsTrue(result.All(_=>resourceSet.ContainsValue(_.Name)), "All names");
            Assert.IsTrue(result.All(_ => _.StartX < _.EndX));
        }
    }
}