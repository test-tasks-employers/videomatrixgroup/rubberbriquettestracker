using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using RubberBriquettesTrackerReporter.DAL;
using RubberBriquettesTrackerReporter.Models;
using RubberBriquettesTrackerReporter.RequestHandlers;
using RubberBriquettesTrackerTests.Infrastructure;

namespace RubberBriquettesTrackerTests
{
    [TestFixture]
    public class ReportHandlerTests
    {
        private static ObjectTrack GetObjectTrack(int? x, DateTime? date = null)
        {
            var objTrackerMock = new Mock<ObjectTrack>();

            objTrackerMock.SetupProperty(_ => _.X, x);
            objTrackerMock.SetupProperty(_ => _.CrtDate, date ?? DateTime.Now);
            return objTrackerMock.Object;
        }

        private RubberBriquettesTrackerContext GetObjectTrackContext(IQueryable<ObjectTrack> data)
        {
            var mockSet = new Mock<DbSet<ObjectTrack>>();
            
            mockSet.As<IAsyncEnumerable<ObjectTrack>>()
                .Setup(m => m.GetEnumerator())
                .Returns(new TestAsyncEnumerator<ObjectTrack>(data.GetEnumerator()));

            mockSet.As<IQueryable<ObjectTrack>>()
                .Setup(m => m.Provider)
                .Returns(new TestAsyncQueryProvider<ObjectTrack>(data.Provider));
            
            mockSet.As<IQueryable<ObjectTrack>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<ObjectTrack>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<ObjectTrack>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            
            var mockContext = new Mock<RubberBriquettesTrackerContext>();
            mockContext.Setup(c => c.ObjectTrack).Returns(mockSet.Object);

            return mockContext.Object;
        }

        private static IDictionary<Zone, ReportItem> GetBlankResult()
        {
            return Zone.DefaultZones.ToDictionary(_ => _,
                _ => new ReportItem() {ZoneName = _.Name, ZoneBriquettesNumber = 0});
        }

        private static void SetResult(IDictionary<Zone, ReportItem> expected, Zone zone, long result)
        {
            expected[zone].ZoneBriquettesNumber = result;
        }

        private void AssertResult(IDictionary<Zone, ReportItem> expected, List<ReportItem> actual)
        {
            Assert.AreEqual(expected.Count, actual.Count);
            foreach (var reportItem in actual)
            {
                Assert.IsTrue(expected.Any(e =>
                        e.Value.ZoneName == reportItem.ZoneName &&
                        e.Value.ZoneBriquettesNumber == reportItem.ZoneBriquettesNumber),
                    $"Не найдено соответствие для (ZoneName = {reportItem.ZoneName}; BriquettesNumber= {reportItem.ZoneBriquettesNumber})");
            }
        }


        [Test]
        public async Task RequestAllHandlerTest()
        {
            var data = new List<ObjectTrack>()
            {
                GetObjectTrack(1000),
                
                GetObjectTrack(5100),
                GetObjectTrack(5200),
                GetObjectTrack(5204),
                GetObjectTrack(5397),
                
                GetObjectTrack(4863),
                GetObjectTrack(5000),
                GetObjectTrack(5013),
                
                GetObjectTrack(2729),
                GetObjectTrack(2800),
                GetObjectTrack(2978),
                
                GetObjectTrack(2008),
                GetObjectTrack(2214),
                
                GetObjectTrack(2215),
            }.AsQueryable();

            var context = GetObjectTrackContext(data);

            var handler = new ReportRequestHandler(context);

            var expected = GetBlankResult();
            SetResult(expected, Zone.MarriageByMetal, 1);
            SetResult(expected, Zone.ZoneOfRegularAppearance1, 4);
            SetResult(expected, Zone.ZoneOfRegularAppearance2, 3);
            SetResult(expected, Zone.BeltDescent, 3);
            SetResult(expected, Zone.Libra, 2);

            var res = await handler.Handle(new ReportQuery(), CancellationToken.None);
            AssertResult(expected, res);
        }
        
        [Test]
        public async Task RequestFilterByDatesHandlerTest()
        {
            var now = DateTime.Now;
            var data = new List<ObjectTrack>()
            {
                GetObjectTrack(1000, now),
                
                GetObjectTrack(5100, now.AddDays(-1)),
                GetObjectTrack(5200, now.AddDays(-2)),
                GetObjectTrack(5204, now.AddHours(-1)),
                GetObjectTrack(5397, now.AddDays(-7)),
                
                GetObjectTrack(4863, now.AddDays(-10)),
                GetObjectTrack(5000, now),
                GetObjectTrack(5013, now),
                
                GetObjectTrack(2729),
                GetObjectTrack(2800),
                GetObjectTrack(2978),
                
                GetObjectTrack(2008),
                GetObjectTrack(2214),
                
                GetObjectTrack(2215),
            }.AsQueryable();

            var context = GetObjectTrackContext(data);

            var handler = new ReportRequestHandler(context);

            var expected = GetBlankResult();
            SetResult(expected, Zone.MarriageByMetal, 1);
            SetResult(expected, Zone.ZoneOfRegularAppearance1, 3);
            SetResult(expected, Zone.ZoneOfRegularAppearance2, 2);

            var res = await handler.Handle(new ReportQuery(){Start = now.AddDays(-2), End = now}, CancellationToken.None);
            AssertResult(expected, res);
        }
    }
}